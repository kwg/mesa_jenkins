#!/usr/bin/python

#import ConfigParser
import multiprocessing
import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), "..", "repos", "mesa_ci"))
import build_support as bs

# needed to preserve case in the options
# class CaseConfig(ConfigParser.SafeConfigParser):
#     def optionxform(self, optionstr):
#         return optionstr


class GLCTSTester(object):
    def __init__(self):
        self.o = bs.Options()
        self.pm = bs.ProjectMap()

    def test(self):
        mv = bs.mesa_version()
        if "17.2" in mv or "17.1" in mv:
            print("NOTICE: GLCTS will NOT be run since the system has Mesa version <17.3")
            return
        t = bs.DeqpTester()
        env = {"MESA_GL_VERSION_OVERRIDE" : "4.6",
               "MESA_GLSL_VERSION_OVERRIDE" : "460"}
        if "iris" in self.o.hardware:
            env["MESA_LOADER_DRIVER_OVERRIDE"] = "iris"
        results = t.test(self.pm.build_root() + "/bin/gl/modules/glcts",
                         bs.GLCTSLister(),
                         env=env)

        o = bs.Options()
        config = bs.get_conf_file(self.o.hardware, self.o.arch, project=self.pm.current_project())
        t.generate_results(results, bs.ConfigFilter(config, o))

    def build(self):
        pass
    def clean(self):
        pass

class SlowTimeout:
    def __init__(self):
        self.hardware = bs.Options().hardware

    def GetDuration(self):
        if "icl" in self.hardware:
            return 180
        return 120

if __name__ == '__main__':
    if not os.path.exists(bs.ProjectMap().project_source_dir("mesa") +
                          "/src/gallium/drivers/iris/meson.build"):
        # iris not supported
        if "iris" in bs.Options().hardware:
            sys.exit(0)

    bs.build(GLCTSTester(), time_limit=SlowTimeout())
